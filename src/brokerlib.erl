%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2022 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 06.09.2022
%%% @doc brokerlib application (queue).
%%%   Provides broker service to get and store messages from producers and send to consumers.
%%%      Consumers are notified if registered to some pid and only if queue was empty. Otherwise consumer should request portion of data itself.
%%%      Queue has name and is automatically created by producer's/consumer's first request ONLY.
%%%   Application could be started or it's modules could be used in other apps as OTP items.
%%%   It uses basiclib and mnesialib. They should be started and setup previously.
%%%      If not then starts itself.
%%%      And setup default output to console and mnesia schema to current node single.
%%%   It use single domain in supvervision tree as default. But could be setup to domain's forest.
%%%   Application could be setup by application:set_env:
%%%      log_destination
%%%          descriptor to define log file folder and prefix.
%%%          Default: {'env','dm'}
%%%      schema_nodes
%%%          define list of distributed nodes for mnesia schema. Used only if mnesialib schema_nodes has not been setup earlier
%%%          Default: [node()]

-module(brokerlib).
-author('Peter Bukashin <tbotc@yandex.ru>').

-behaviour(application).

-export([
    start/2,
    stop/1
]).

-export([
    add_domain/1,
    delete_domain/1,
    get_domain_childspec/2,
    get_regname/1
]).

-export([
    setup_queues/2,
    enqueue/3, enqueue/4,
    enqueue_list/3, enqueue_list/4,
    dequeue/3, dequeue/4,
    peek/3, peek/4,
    delete/3, delete/4,
    register_consumer/3, register_consumer/4
]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public
%% ====================================================================

%% -------------------------------------------
%% Append domain supervision tree
%% -------------------------------------------
-spec add_domain(Domain::binary() | atom()) -> {ok,Pid::pid(),RegName::atom()} | ignore | {error,Reason::term()}.
%% -------------------------------------------
add_domain(Domain) when is_binary(Domain); is_atom(Domain) ->
    RegName = get_regname(Domain),
    ChildSpec = get_domain_childspec(Domain,RegName),
    case ?SUPV:start_child(ChildSpec) of
        {ok,Pid} -> {ok,Pid,RegName};
        ignore -> ignore;
        {error,_}=Err -> Err
    end.

%% -------------------------------------------
%% Remove domain supervision tree. Used only if domain added to dmlib application's supervisor by add_domain/1
%% -------------------------------------------
-spec delete_domain(Domain::binary() | atom()) -> ok | {error,Reason::term()}.
%% -------------------------------------------
delete_domain(Domain) when is_binary(Domain); is_atom(Domain) ->
    LinkName = ?DSUPV:get_linkname(Domain),
    case ?SUPV:get_childspec(LinkName) of
        {ok,_} ->
            ?SUPV:terminate_child(LinkName),
            ?SUPV:delete_child(LinkName);
        {error,_}=Err -> Err
    end.

%% -------------------------------------------
%% Return childspec for domain's supervisor (to start under external supervisor)
%% -------------------------------------------
-spec get_domain_childspec(Domain::binary() | atom(), RegName::atom()) -> ChildSpec::tuple().
%% -------------------------------------------
get_domain_childspec(Domain,RegName) when (is_binary(Domain) orelse is_atom(Domain)) andalso is_atom(RegName) ->
    LinkName = ?DSUPV:get_linkname(Domain),
    Opts = [{'regname',RegName},
            {'domain',Domain}],
    {LinkName, {?DSUPV, start_link, [Opts]}, permanent, 1000, supervisor, [?DSUPV]}.

%% -------------------------------------------
%% Return default regname for domain's facade server
%% -------------------------------------------
-spec get_regname(Domain::binary() | atom()) -> RegName::atom().
%% -------------------------------------------
get_regname(Domain) when is_binary(Domain); is_atom(Domain) ->
    ?DSRV:get_regname(Domain).

%% -------------------------------------------
%% Load new classes metadata for selected domain. NewClasses
%% Item :: binary() | #{queuename}
%% -------------------------------------------
-spec setup_queues(Domain::binary() | atom(), [Item::map()]) -> ok.
%% -------------------------------------------
setup_queues(Domain,[_|_]=Queues) when is_binary(Domain); is_atom(Domain)->
    RegName = ?DSRV:get_regname(Domain),
    case whereis(RegName) of
        Pid when is_pid(Pid) -> gen_server:cast(Pid, {setup_queues,Queues});
        undefined -> {error,not_found}
    end.

%% -------------------------------------------
%% Produce message to broker
%% -------------------------------------------
-spec enqueue(RegNameOrPid::atom() | pid(), QueueName::binary(), Message::term()) -> ok.
enqueue(RegNameOrPid, QN, Msg) when is_binary(QN) ->
    enqueue(RegNameOrPid, fun(_) -> ok end, QN, Msg).
%% -------------------------------------------
-spec enqueue(RegNameOrPid::atom() | pid(), FunReply::function(), QueueName::binary(), Message::term()) -> ok.
enqueue(RegNameOrPid, FunReply, QN, Msg) when is_function(FunReply,1), is_binary(QN) ->
    gen_server:cast(RegNameOrPid,{enqueue,FunReply,QN,Msg}).

%% -------------------------------------------
%% Produce list of messages to broker's queue
%% -------------------------------------------
-spec enqueue_list(RegNameOrPid::atom() | pid(), QueueName::binary(), [Message::term()]) -> ok.
enqueue_list(RegNameOrPid, QN, [_|_]=Msgs) when is_binary(QN) ->
    enqueue_list(RegNameOrPid, fun(_) -> ok end, QN, Msgs).
%% -------------------------------------------
-spec enqueue_list(RegNameOrPid::atom() | pid(), FunReply::function(), QueueName::binary(), [Message::term()]) -> ok.
enqueue_list(RegNameOrPid, FunReply, QN, [_|_]=Msgs) when is_function(FunReply,1), is_binary(QN) ->
    gen_server:cast(RegNameOrPid,{enqueue_list,FunReply,QN,Msgs}).

%% -------------------------------------------
%% Consume message(s) from broker's queue
%% OptsMap :: #{ref, count}
%% -------------------------------------------
-spec dequeue(RegNameOrPid::atom() | pid(), QueueName::binary(), OptsMap::map())
      -> {ok,Messages::[Msg::term()]} | {error,Reason::term()}.
%% -------------------------------------------
dequeue(RegNameOrPid, QN, OptsMap) when is_binary(QN), is_map(OptsMap) ->
    gen_server:call(RegNameOrPid,{dequeue,QN,OptsMap}).

%% -------------------------------------------
-spec dequeue(RegNameOrPid::atom() | pid(), FunReply::function(), QueueName::binary(), OptsMap::map())
      -> {ok,Messages::[Msg::term()]} | {error,Reason::term()}.
%% -------------------------------------------
dequeue(RegNameOrPid, FunReply, QN, OptsMap) when is_function(FunReply,1), is_binary(QN), is_map(OptsMap) ->
    gen_server:cast(RegNameOrPid,{dequeue,FunReply,QN,OptsMap}).

%% -------------------------------------------
%% Peek message(s) from broker's queue
%% OptsMap :: #{ref, count}
%% -------------------------------------------
-spec peek(RegNameOrPid::atom() | pid(), QueueName::binary(), OptsMap::map())
      -> {ok,Msg::term(),Key::integer()} | false | {error,Reason::term()}.
%% -------------------------------------------
peek(RegNameOrPid, QN, OptsMap) when is_binary(QN), is_map(OptsMap) ->
    gen_server:call(RegNameOrPid,{peek,QN,OptsMap}).

%% -------------------------------------------
-spec peek(RegNameOrPid::atom() | pid(), FunReply::function(), QueueName::binary(), OptsMap::map())
      -> {ok,Msg::term(),Key::integer()} | false | {error,Reason::term()}.
%% -------------------------------------------
peek(RegNameOrPid, FunReply, QN, OptsMap) when is_function(FunReply,1), is_binary(QN), is_map(OptsMap) ->
    gen_server:cast(RegNameOrPid,{peek,FunReply,QN,OptsMap}).

%% -------------------------------------------
%% Peek message(s) from broker's queue
%% OptsMap :: #{ref, count}
%% -------------------------------------------
-spec delete(RegNameOrPid::atom() | pid(), QueueName::binary(), OptsMap::map())
      -> ok | false | {error,Reason::term()}.
%% -------------------------------------------
delete(RegNameOrPid, QN, OptsMap) when is_binary(QN), is_map(OptsMap) ->
    gen_server:call(RegNameOrPid,{delete,QN,OptsMap}).

%% -------------------------------------------
-spec delete(RegNameOrPid::atom() | pid(), FunReply::function(), QueueName::binary(), OptsMap::map())
      -> ok | false | {error,Reason::term()}.
%% -------------------------------------------
delete(RegNameOrPid, FunReply, QN, OptsMap) when is_function(FunReply,1), is_binary(QN), is_map(OptsMap) ->
    gen_server:cast(RegNameOrPid,{delete,FunReply,QN,OptsMap}).

%% -------------------------------------------
%% Register consumer to domain's server. Result would be passed to FunReply/1.
%%   For getting wakeup notifies
%% OptsMap :: #{pid, wakeup_message, rolename}
%% -------------------------------------------
-spec register_consumer(RegNameOrPid::atom() | pid(), QueueName::binary(), OptsMap::map())
      -> {ok,Ref::reference(),MonPid::pid()} | {error,Reason::term()}.
%% -------------------------------------------
register_consumer(RegNameOrPid, QN, OptsMap) when is_binary(QN), is_map(OptsMap) ->
    gen_server:call(RegNameOrPid,{register_consumer,QN,OptsMap}).

%% -------------------------------------------
-spec register_consumer(RegNameOrPid::atom() | pid(), FunReply::function(), QueueName::binary(), OptsMap::map())
      -> {ok,Ref::reference(),MonPid::pid()} | {error,Reason::term()}.
%% -------------------------------------------
register_consumer(RegNameOrPid, FunReply, QN, OptsMap) when is_function(FunReply,1), is_binary(QN), is_map(OptsMap) ->
    gen_server:cast(RegNameOrPid,{register_consumer,FunReply,QN,OptsMap}).

%% ===================================================================
%% Callback functions (application)
%% ===================================================================

%% -------------------------------------
%% @doc Starts application
%% -------------------------------------
start(_, _) ->
    setup_start_ts(),
    setup_dependencies(),
    ensure_deps_started(),
    ?SUPV:start_link(undefined).

%% -------------------------------------
%% @doc Stops application.
%% -------------------------------------
stop(_State) ->
    ok.

%% ====================================================================
%% Internal
%% ====================================================================

%% --------------------------------------
%% @private
%% setup start_utc to applicaiton's env for monitor purposes
%% --------------------------------------
setup_start_ts() ->
    application:set_env(?APP,start_utc,?BU:timestamp()).

%% @private
setup_dependencies() ->
    % basiclib
    case ?BU:get_env(?BASICLIB, 'log_function',undefined) of
        undefined -> ?BU:set_env(?BASICLIB, log_function, fun(_FileKey,{Fmt,Args}) -> io:format(Fmt++"\n",Args);
            (_FileKey,Txt) -> io:format(Txt,[])
                                                          end);
        _ -> ok
    end,
    % mnesialib
    case ?BU:get_env(?MNESIALIB, 'schema_nodes',undefined) of
        undefined ->
            SchemaNodes = case ?CFG:schema_nodes(undefined) of
                              [_|_]=Nodes -> Nodes;
                              _ -> [node()]
                          end,
            ?BU:set_env(?MNESIALIB, 'schema_nodes', SchemaNodes);
        _ -> ok
    end,
    ok.

%% --------------------------------------
%% @private
%% starts deps applications, that out of app link
%% --------------------------------------
ensure_deps_started() ->
    {ok,_} = application:ensure_all_started(?BASICLIB, permanent),
    {ok,_} = application:ensure_all_started(?MNESIALIB, permanent),
    {ok,_} = application:ensure_all_started(brod, permanent),
    ok.