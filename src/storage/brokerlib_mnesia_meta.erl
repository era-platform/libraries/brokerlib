%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2022 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 07.09.2022
%%% @doc

-module(brokerlib_mnesia_meta).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([
    table_name/2,
    ensure_table/3
]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% -------------------------
%% Build table
%% -------------------------
table_name(Domain,QueueName) ->
    ?BU:to_atom_new(?BU:str("~ts:tab;dom:~ts;qn:~ts",[?APP,Domain,binary:replace(QueueName,<<"/">>,<<"|">>,[global])])).

%% -------------------------
%% Make table in Mnesia for domain-model-class
%% -------------------------
ensure_table(Domain,QueueName,Meta) ->
    TableName = table_name(Domain,QueueName),
    TableMeta = ?STORE:get_table_meta(TableName),
    Opts = TableMeta#{'app_name' => ?APP,
                      'app_version' => undefined,
                      'load_timeout' => 20000,
                      'update_function' => fun({_AppName,_AppVersion},Rec0) -> Rec0 end},
    case ensure_mnesialib_started() of
        ok ->
            case ?MNESIALIB:ensure_table(TableName, maps:get(record_name,TableMeta), maps:get(record_fields,TableMeta), Opts) of
                ok -> ok;
                {error,Reason}=Err ->
                    ?LOG('$error',"~ts. '~ts' * '~ts' mnesia load table error: ~120tp...",[?APP,Domain,QueueName,Reason]),
                    Err;
                {retry_after,Timeout,Reason} ->
                    ?LOG('$info',"~ts. '~ts' * '~ts' wait for mnesia to ensure and load table... (~ts)",[?APP,Domain,QueueName,Reason]),
                    timer:sleep(Timeout),
                    ensure_table(Domain,QueueName,Meta)
            end;
        {retry_after,Timeout,Reason} ->
            ?LOG('$info',"~ts. '~ts' * '~ts' wait for mnesialib to start properly... (~ts)",[?APP,Domain,QueueName,Reason]),
            timer:sleep(Timeout),
            ensure_table(Domain,QueueName,Meta)
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================

ensure_mnesialib_started() ->
    case lists:keyfind(?MNESIALIB,1,application:which_applications()) of
        false ->
            application:ensure_all_started(?BASICLIB),
            application:ensure_all_started(?MNESIALIB),
            {retry_after,1000,<<"Wait for mnesia start">>};
        {_,_,_} ->
            case ?MNESIALIB:get_schema_status() of
                'running' -> ok;
                _ ->
                    F = fun F(I) when I =< 0 -> ok;
                            F(I) ->
                                case ?MNESIALIB:get_schema_status() of
                                    'running' -> ok;
                                    _ -> timer:sleep(100), F(I-1)
                                end
                        end,
                    F(10),
                    case ?MNESIALIB:get_schema_status() of
                        'running' -> ok;
                        _ -> {retry_after, 5000, "Wait for mnesia to sync schema"}
                    end
            end
    end.