%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2022 Peter Bukashin
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc Storage module to work with mnesia table in transaction manner
%%%      Mnesia defines two modes of transaction types: write and sticky_write
%%%         sticky_write should be used only when replication for fault tolerance purposes mainly
%%%         write - should be used when several replicas are in parallel work (for read and write)

-module(brokerlib_storage_tran).

-export([
    get_table_meta/1,
    %
    put/3, put_multi/3,
    get/1,
    get_data_pack/2,
    get_first_key/1,
    get_last_key/1,
    del/1,
    del_unuse/1,
    del_all/1,
    del_list_keys/2,
    del_n_items/2,
    %
    get_queue_size/1
]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("app.hrl").
-include("brokerlib_storage.hrl").
-include_lib("stdlib/include/ms_transform.hrl").

-define(WriteLock, write). % write | sticky_write (replication for fault tolerance purposes mainly)

%% ====================================================================
%% Public functions
%% ====================================================================

%% ----------------------------------
%% table meta for creating
%% ----------------------------------
get_table_meta(Table) ->
    #{
        table_name => Table,
        record_name => queuedata,
        record_fields => record_info(fields, queuedata),
        index_fields => [],
        %update_function => fun({_AppName,_AppVersion},Rec0) -> Rec0 end,
        type => ordered_set
        %unsplit_method => full_merge
    }.

%% ----------------
%% Put value into table. Generate next EventId.
%% ----------------
-spec put(TableName::atom(),Data::term(),LastEventId::integer()) -> {ok,NewEventId::integer()} | {error,Reason::term()}.
%% ----------------
put(TableName,Data,_LastEventId) ->
    tran(fun() ->
                mnesia:lock({table,TableName},?WriteLock),
                EventId = 1 + case mnesia:last(TableName) of
                                  '$end_of_table' -> 0;
                                  Key when is_integer(Key) -> Key
                              end,
                Rec = #queuedata{key=EventId, value=Data, phash=0},
                ok = mnesia:write(TableName, Rec, ?WriteLock),
                {ok,EventId}
         end).

%% ----------------
%% Put values into table. Generate next EventIds
%% ----------------
-spec put_multi(TableName::atom(),Data::[term()],EventId::integer()) -> {ok,NewEventId::integer()} | {error,Reason::term()}.
%% ----------------
put_multi(_,[],LastEventId) -> {ok,LastEventId};
put_multi(TableName,[_|_]=DataList,_LastEventId) ->
    tran(fun() ->
                mnesia:lock({table,TableName},?WriteLock),
                LastEventId = case mnesia:last(TableName) of
                                 '$end_of_table' -> 0;
                                 Key when is_integer(Key) -> Key
                              end,
                LastEventId1 = lists:foldl(fun(Data,Acc) ->
                                                EventId = Acc + 1,
                                                Rec = #queuedata{key=EventId, value=Data, phash=0},
                                                ok = mnesia:write(TableName, Rec, ?WriteLock),
                                                EventId
                                           end, LastEventId, DataList),
                {ok,LastEventId1}
         end).

%% ----------------
get([Key,TableName]) ->
    case tran(fun() -> mnesia:read(TableName, Key) end) of
        {aborted,_Reason} -> {error,[]};
        [] -> {ok,[]};
        [#queuedata{value=Data}] -> {ok, Data};
        [#queuedata{value=Data}|_] -> {ok, Data}
    end;
get(_) -> {error, invalid_args}.

%% ----------------
get_data_pack(_,Cnt) when Cnt=<0 -> {[],0,[]};
get_data_pack(TableName,Cnt) ->
    Fun = fun() ->
                FirstKey = mnesia:first(TableName),
                do_get_data_pack(TableName,FirstKey,Cnt,0,[],[])
          end,
    case tran(Fun) of
        {atomic,X} -> X
    end.
%% @private
do_get_data_pack(_,'$end_of_table',_,DataCnt,Keys,Data) -> {lists:reverse(Data),DataCnt,lists:reverse(Keys)};
do_get_data_pack(_,_,ReqCnt,DataCnt,Keys,Data) when ReqCnt==DataCnt -> {Data,DataCnt,Keys};
do_get_data_pack(TableName,CurrentKey,ReqCnt,DataCnt,Keys,Data) ->
    case mnesia:read(TableName,CurrentKey) of
        [#queuedata{value=V}] ->
            NextKey = mnesia:next(TableName,CurrentKey),
            do_get_data_pack(TableName,NextKey,ReqCnt,DataCnt+1,[CurrentKey|Keys],[V|Data]);
        [] ->
            FirstKey = mnesia:first(TableName),
            do_get_data_pack(TableName,FirstKey,ReqCnt,DataCnt,Keys,Data)
    end.

%% ----------------
get_first_key(TableName) ->
    case tran(fun() -> mnesia:first(TableName) end) of
        {aborted,_Reason} -> {error,0};
        '$end_of_table' -> {empty,0};
        Key -> {ok,Key}
    end.

% ----------------
get_last_key(TableName) ->
    case tran(fun() -> mnesia:last(TableName) end) of
        {aborted,_Reason} -> {error,0};
        '$end_of_table' -> {empty,0};
        Key -> {ok,Key}
    end.

% ----------------
get_queue_size([TableName]) ->
    case mnesia:table_info(TableName, size) of
        {aborted,Err} -> {error,Err};
        Size -> {ok,Size}
    end.

% ----------------
del([Key,Table]) ->
    tran(fun() -> mnesia:delete(Table, Key, ?WriteLock) end);
del(_) -> {error, invalid_args}.

% ----------------
del_unuse(Info) -> tran(fun() -> do_del_unuse(Info) end).
%% @private
do_del_unuse([TargetKey,TargetKey,_Table]) -> ok;
do_del_unuse([MinKey,TargetKey,Table]) ->
    mnesia:delete(Table, MinKey, ?WriteLock),
    do_del_unuse([MinKey+1,TargetKey,Table]).

% ----------------
del_list_keys(Keys,Table) -> tran(fun() -> do_del_list_keys(Keys,Table) end).
%% @private
do_del_list_keys([],_Table) -> ok;
do_del_list_keys([Key|Tail],Table) ->
    mnesia:delete(Table, Key, ?WriteLock),
    do_del_list_keys(Tail,Table).

%% ---
del_n_items(Table,Cnt) -> tran(fun() -> do_del_n_items(Table,Cnt) end).
%% @private
do_del_n_items(_,0) -> ok;
do_del_n_items(TableName,Cnt) ->
    FirstKey = mnesia:first(TableName),
    do_del_n_items(TableName,FirstKey,Cnt).
%% @private
do_del_n_items(_,'$end_of_table',_) -> ok;
do_del_n_items(_,_,Cnt) when Cnt==0 -> ok;
do_del_n_items(TableName,CurrentKey,Cnt) ->
    mnesia:delete(TableName, CurrentKey, ?WriteLock),
    NextKey = mnesia:next(TableName,CurrentKey),
    do_del_n_items(TableName,NextKey,Cnt-1).

% ----------------
del_all([Table]) ->
    ?LOG('$info',"~ts. del_all from '~ts'",[?APP,Table]),
    mnesia:clear_table(Table),
    ok;
del_all(_) -> {error, invalid_args}.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% @private
tran(Fun) when is_function(Fun,0) ->
    case mnesia:transaction(Fun) of
        {atomic,Res} -> Res;
        Other -> Other
    end.