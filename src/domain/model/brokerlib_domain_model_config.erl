%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 06.09.2022
%%% @doc routines of static queue changes appliance

-module(brokerlib_domain_model_config).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([
    refresh/2,
    start_queue_ex/2
]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

refresh(NewItems, #dstate{domain=Domain,static_items=OldItems}=State) ->
    % nesting is external work
    % filtering by role is external work
    % removing automodificated fields is external work
    % just decorate by domain
    NewItems1 = decorate_items(Domain,NewItems),
    {ok,Created,Deleted,_Unchanged,ModifiedEx} = ?BU:juxtapose_ex(OldItems,NewItems1,fun(Item) -> maps:get(queuename,Item) end),
    ?OUT('$info', "~ts. '~ts' refresh static: ~120p, ~120p, ~120p, ~120p", [?APP,Domain,length(Created),length(Deleted),length(_Unchanged),length(ModifiedEx)]),
    % TODO ICP: handle if something wrong
    State1 = delete(Deleted,State),
    State2 = create(Created,State1),
    State3 = modify(ModifiedEx,State2),
    {ok,State3#dstate{static_items=NewItems1}}.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% ------------------------------
%% Decorate by domain
%% ------------------------------
decorate_items(Domain,Items) -> [decorate_item(Domain,Item) || Item <- Items].
decorate_item(Domain,QN) when is_binary(QN) -> decorate_item(Domain,#{'queuename' => QN});
decorate_item(Domain,Item) -> Item#{'domain' => Domain}.

%% ====================================================================
%% Apply changes routines
%% ====================================================================

%% ----------------------------------
%% starts OTP subtree to handle new queue
%% ----------------------------------
create(List,State) ->
    lists:foldl(fun(Item,StateX) -> start_queue(Item,StateX) end, State, List).

%% @private
start_queue(Item,#dstate{queues=Q}=State) ->
    QueueName = maps:get(queuename,Item),
    case start_queue_ex(Item,State) of
        {ok,State1} -> State1;
        {error,_} -> State#dstate{queues=maps:without([QueueName],Q)}
    end.

%% @public
start_queue_ex(Item,#dstate{domain=Domain,queues=Q}=State) ->
    QueueName = maps:get(queuename,Item),
    Opts = [{'domain',Domain},
            {'queuename',QueueName},
            {'queue_meta',Item}],
    case do_start_queue(Domain,QueueName,Opts) of
        ok -> {ok,State#dstate{queues=Q#{QueueName => {undefined,undefined}}}};
        {error,_}=Err -> Err
    end.

%% @private
%% Starts queue supervisor
do_start_queue(Domain,Queue,Opts) ->
    {SM,SF,SA} = {?QueueSupv,start_link,[Opts]},
    ChildSupv = ?QueueSupv:get_linkname(Domain,Queue),
    ParentSupv = ?DSUPV:get_regname(Domain),
    case supervisor:get_childspec(ParentSupv, ChildSupv) of
        {ok, _Spec} -> ok;
        {error, _} ->
            ChildSpec={ChildSupv, {SM,SF,SA}, permanent, 1000, supervisor, [?QueueSupv]},
            start_child(ParentSupv,ChildSpec)
    end.

%% @private
start_child(ParentSupv,ChildSpec) ->
    case supervisor:start_child(ParentSupv,ChildSpec) of
        {ok, _} -> ok;
        {ok, _, _} -> ok;
        {error,{{shutdown,{failed_to_start_child,_,_}},_}}=Err ->
            ?LOG('$error',"Start queue child ~120tp got ~120tp, stop node...",[ChildSpec,Err]),
            ?BU:stop_node(1000),
            Err;
        Err ->
            ?LOG('$error',"Start queue child ~120tp got ~120tp, stop node...",[ChildSpec,Err]),
            ?BU:stop_node(1000),
            Err
    end.

%% ----------------------------------
%% removes OTP subtree for queue
%% ----------------------------------
delete(List,#dstate{queues=Q}=State) ->
    Q1 = lists:foldl(fun(Item,Acc) ->
                            stop_queue(Item,State),
                            maps:without([maps:get(queuename,Item)],Acc)
                     end, Q, List),
    State#dstate{queues=Q1}.

%% @private
stop_queue(Item,#dstate{domain=Domain}) ->
    QueueName = maps:get(queuename,Item),
    do_stop_queue(Domain,QueueName).

%% @private
%% Stops queue supervisor
do_stop_queue(Domain,QueueName) ->
    ChildSupv = ?QueueSupv:get_linkname(Domain,QueueName),
    ParentSupv = ?DSUPV:get_regname(Domain),
    case supervisor:get_childspec(ParentSupv,ChildSupv) of
        {ok, _Spec} ->
            supervisor:terminate_child(ParentSupv,ChildSupv),
            supervisor:delete_child(ParentSupv,ChildSupv);
        {error, _} -> ok
    end.

%% ----------------------------------
%% modify for queue
%% TODO ICP: optimize simple changes modifications.
%% ----------------------------------
modify(ListEx,State) ->
    F = fun({ItemOld,ItemNew},StateX) ->
            stop_queue(ItemOld,StateX),
            start_queue(ItemNew,StateX)
        end,
    lists:foldl(F, State, ListEx).
