%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2022 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 06.09.2022
%%% @doc General facade server of domain's broker
%%%        Has global name, handles all outside requests.
%%%        Routes request to queue servers.
%%%      Opts:
%%%        regname, domain

-module(brokerlib_domain_srv).
-author('Peter Bukashin <tbotc@yandex.ru>').

-behaviour(gen_server).

-export([start_link/2]).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2, code_change/3]).

-export([
    get_regname/1,
    cast/2
]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("app.hrl").

-record(waitor, {
    queuename :: binary(),
    type :: 'enqueue' | 'dequeue',
    fun_apply :: function(),
    expire_ts :: non_neg_integer()
}).

-define(GetWaitTimeout, 3000).
-define(PutWaitTimeout, 60000).

%% ====================================================================
%% API functions
%% ====================================================================

start_link(RegName, Opts) ->
    gen_server:start_link({local, RegName}, ?MODULE, [{'regname', RegName}|Opts], []).

get_regname(Domain) when is_binary(Domain); is_atom(Domain) ->
    ?BU:to_atom_new(?BU:strbin("~ts:srv;dom:~ts",[?APP,Domain])).

cast(Domain,Message) when is_binary(Domain) ->
    cast(get_regname(Domain),Message);
cast(RegName,Message) when is_atom(RegName) ->
    gen_server:cast(RegName, Message).

%% ====================================================================
%% Callback functions
%% ====================================================================

%% ------------------------------
%% Init
%% ------------------------------
init(Opts) ->
    [RegName, Domain] = ?BU:extract_required_props(['regname','domain'], Opts),
    Ref = make_ref(),
    State = #dstate{regname = RegName,
                    domain = Domain,
                    queues = #{},
                    statref = Ref},
    self() ! {init,Ref},
    ?LOG('$info', "~ts. '~ts' facade inited", [?APP, Domain]),
    {ok, State}.

%% ------------------------------
%% Call
%% ------------------------------

%% ----
%% returns current node
handle_call({get_node}, _From, State) ->
    {reply, {node(), self()}, State};

%% ----
%% print state
handle_call({print}, _From, #dstate{domain=Domain}=State) ->
    ?LOG('$force', " ~ts. '~ts' facade state: ~n\t~120tp", [?APP, Domain, State]),
    {reply, ok, State};

%% ----
%% restart gen_serv
handle_call({restart}, _From, State) ->
    {stop, restart, ok, State};

%% ----
%% Sync get operations
%% register_consumer
%% dequeue by consumer
%% ----
handle_call({Operation,QueueName,OptsMap}, From, State)
  when Operation=='register_consumer'; Operation=='dequeue'; Operation=='peek'; Operation=='delete' ->
    FunReply = fun(Reply) -> gen_server:reply(From,Reply) end,
    handle_cast({Operation,FunReply,QueueName,OptsMap}, State);

%% ----
%% other
handle_call(_Msg, _From, State) ->
    {noreply, State}.

%% ------------------------------
%% Cast
%% ------------------------------

%% -----------
%% Async get operations
%% register_consumer
%% dequeue by consumer
%% -----------
handle_cast({Operation,FunReply,QueueName,OptsMap}, #dstate{queues=Q}=State)
  when Operation=='register_consumer'; Operation=='dequeue'; Operation=='peek'; Operation=='delete' ->
    CmdMsg = {Operation,FunReply,OptsMap},
    case maps:get(QueueName,Q,undefined) of
        undefined ->
            case start_queue(QueueName,State) of
                {error,_}=Err -> {reply,Err,State};
                {ok,#dstate{queues=Q}} ->
                    Err = {error,{internal_error,<<"Queue unavailable">>}},
                    FunReply(Err);
                {ok,State1} ->
                    NowTS = ?BU:timestamp(),
                    FunApply = fun(Pid) -> gen_server:cast(Pid,CmdMsg) end,
                    State2 = append_waitor(QueueName,'dequeue',FunApply,NowTS,?GetWaitTimeout,State1),
                    {noreply,State2}
            end;
        {X,Y} when X==undefined; Y==undefined ->
            NowTS = ?BU:timestamp(),
            FunApply = fun(Pid) -> gen_server:cast(Pid,CmdMsg) end,
            State1 = append_waitor(QueueName,'dequeue',FunApply,NowTS,?GetWaitTimeout,State),
            {noreply,State1};
        {_,DequeuePid} when is_pid(DequeuePid) ->
            % TODO: What if Pid is down and not up yet? Do cast through queue facade server
            gen_server:cast(DequeuePid,CmdMsg),
            {noreply,State}
    end;

%% -----------
%% enqueue by producer
%% -----------
handle_cast({enqueue,FunReply,QueueName,Message}, #dstate{queues=Q}=State) when is_function(FunReply,1) ->
    CmdMsg = {enqueue,FunReply,Message},
    case maps:get(QueueName,Q,undefined) of
        undefined ->
            case start_queue(QueueName,State) of
                {error,_}=Err ->
                    FunReply(Err),
                    {noreply,State};
                {ok,#dstate{queues=Q}} ->
                    FunReply({error,{internal_error,<<"Queue unavailable">>}}),
                    {noreply,State};
                {ok,State1} ->
                    NowTS = ?BU:timestamp(),
                    FunApply = fun(Pid) -> gen_server:cast(Pid,CmdMsg) end,
                    State2 = append_waitor(QueueName,'enqueue',FunApply,NowTS,?PutWaitTimeout,State1),
                    {noreply,State2}
            end;
        {X,Y} when X==undefined; Y==undefined ->
            NowTS = ?BU:timestamp(),
            FunApply = fun(Pid) -> gen_server:cast(Pid,CmdMsg) end,
            State1 = append_waitor(QueueName,'enqueue',FunApply,NowTS,?PutWaitTimeout,State),
            {noreply,State1};
        {EnqueuePid,_} ->
            % TODO: What if Pid is down and not up yet? Do cast through queue facade server
            gen_server:cast(EnqueuePid,CmdMsg),
            {noreply,State}
    end;

handle_cast({enqueue_list,FunReply,QueueName,Messages}, #dstate{queues=Q}=State) when is_function(FunReply,1) ->
    CmdMsg = {enqueue_list,FunReply,Messages},
    case maps:get(QueueName,Q,undefined) of
        undefined ->
            case start_queue(QueueName,State) of
                {error,_}=Err ->
                    FunReply(Err),
                    {noreply,State};
                {ok,#dstate{queues=Q}} ->
                    FunReply({error,{internal_error,<<"Queue unavailable">>}}),
                    {noreply,State};
                {ok,State1} ->
                    NowTS = ?BU:timestamp(),
                    FunApply = fun(Pid) -> gen_server:cast(Pid,CmdMsg) end,
                    State2 = append_waitor(QueueName,'enqueue',FunApply,NowTS,?PutWaitTimeout,State1),
                    {noreply,State2}
            end;
        {X,Y} when X==undefined; Y==undefined ->
            NowTS = ?BU:timestamp(),
            FunApply = fun(Pid) -> gen_server:cast(Pid,CmdMsg) end,
            State1 = append_waitor(QueueName,'enqueue',FunApply,NowTS,?PutWaitTimeout,State),
            {noreply,State1};
        {EnqueuePid,_} ->
            % TODO: What if Pid is down and not up yet? Do cast through queue facade server
            gen_server:cast(EnqueuePid,CmdMsg),
            {noreply,State}
    end;

%% --------------
%% TODO: make async
%% --------------
handle_cast({setup_queues,NewQueues}, #dstate{static_hc=HC}=State) when is_list(NewQueues) ->
    case erlang:phash2(NewQueues) of
        HC -> {noreply, State};
        HC1 ->
            {ok,State1} = ?ModelCfg:refresh(NewQueues, State),
            State2 = State1#dstate{static_hc=HC1},
            State3 = apply_waitors(State2),
            {noreply, State3}
    end;

%% --------------
%% when queue worker started
%% --------------
handle_cast({started,_SyncRef,QueueName,SrvType,Pid}, #dstate{queues=Q}=State) when SrvType=='enqueue';SrvType=='dequeue' ->
    State1 = case maps:get(QueueName,Q,undefined) of
                 undefined -> State;
                 {_,De} when SrvType=='enqueue' -> State#dstate{queues=Q#{QueueName => {Pid,De}}};
                 {En,_} when SrvType=='dequeue' -> State#dstate{queues=Q#{QueueName => {En,Pid}}}
             end,
    State2 = apply_waitors(State1),
    {noreply, State2};

%% --------------
%% other
%% --------------
handle_cast(_Msg, State) ->
    {noreply, State}.

%% ------------------------------
%% Info
%% ------------------------------

handle_info({init,Ref}, #dstate{statref=Ref}=State) ->
    State1 = start_stat_timer(State),
    {noreply,State1};

%% --------------
handle_info({stat,Ref},#dstate{statref=Ref,domain=Domain}=State) ->
    ?LOG('$trace', "~ts. '~ts' STAT. facade handled ? requests", [?APP, Domain]),
    State1 = start_stat_timer(State),
    {noreply,State1};

%% --------------
handle_info(_Info, State) ->
    {noreply, State}.

%% ------------------------------
%% Terminate
%% ------------------------------
terminate(_Reason, #dstate{}=_State) ->
    ok.

%% ------------------------------
%% Code change
%% ------------------------------
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% --------
%% @private
start_queue(QueueName,#dstate{domain=Domain}=State) ->
    QueueMeta = #{queuename => QueueName,
                  domain => Domain},
    ?ModelCfg:start_queue_ex(QueueMeta, State).

%% --------
%% @private
start_stat_timer(#dstate{}=State) ->
    Ref1 = make_ref(),
    State#dstate{statref=Ref1,
                 stattimerref=erlang:send_after(60000,self(),{stat,Ref1})}.

%% --------
%% @private
%% Add call request to waitors of queue load
append_waitor(QueueName,Type,FunApply,NowTS,Timeout,#dstate{waitors=W}=State) ->
    ExpireTS = NowTS + Timeout,
    Waitor = #waitor{queuename = QueueName,
                     type = Type,
                     fun_apply = FunApply,
                     expire_ts = ExpireTS},
    W1 = lists:filter(fun(#waitor{expire_ts=ExpTS}) -> ExpTS =< NowTS end, W),
    State#dstate{waitors=[Waitor|W1]}.

%% Apply waitors when something changed in static queues list
apply_waitors(#dstate{waitors=W,queues=Q}=State) ->
    NowTS = ?BU:timestamp(),
    W1 = lists:foldr(fun(#waitor{expire_ts=ExpTS},Acc) when ExpTS =< NowTS -> Acc;
                        (#waitor{queuename=QN,type=Type,fun_apply=FunApply}=Waitor,Acc) ->
                            case maps:get(QN,Q) of
                                {undefined,_} -> [Waitor|Acc];
                                {_,undefined} -> [Waitor|Acc];
                                {Pid,_} when Type=='enqueue' -> FunApply(Pid), Acc;
                                {_,Pid} when Type=='dequeue' -> FunApply(Pid), Acc
                            end
                     end, [], W),
    State#dstate{waitors=W1}.
