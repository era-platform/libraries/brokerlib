%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2022 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 06.09.2022
%%% @doc Supervisor of domain's broker service
%%%      Supervises group of relevant queue processes concerned to domain
%%%      Opts:
%%%        domain::binary(),
%%%        regname::atom()

-module(brokerlib_domain_supv).
-author('Peter Bukashin <tbotc@yandex.ru>').

-behaviour(supervisor).

-export([start_link/1, get_linkname/1, get_regname/1]).
-export([start_child/1, restart_child/1, terminate_child/1, delete_child/1, get_childspec/1]).
-export([init/1]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% API functions (start)
%% ====================================================================

%% starts supv
start_link(Opts) ->
    Domain = ?BU:get_by_key('domain', Opts),
    RegName = get_linkname(Domain),
    supervisor:start_link({local,RegName}, ?MODULE, Opts).

%% returns name of supv for linking to top supv
get_linkname(Domain) when is_binary(Domain); is_atom(Domain) ->
    get_regname(Domain).

%% returns reg name of supv
get_regname(Domain) when is_binary(Domain); is_atom(Domain) ->
    ?BU:to_atom_new(?BU:strbin("~ts:supv;dom:~ts",[?APP,Domain])).

%% ====================================================================
%% API functions (ext management)
%% ====================================================================

start_child(ChildSpec) ->
    supervisor:start_child(?MODULE, ChildSpec).

restart_child(Id) ->
    supervisor:restart_child(?MODULE, Id).

terminate_child(Id) ->
    supervisor:terminate_child(?MODULE, Id).

delete_child(Id) ->
    supervisor:delete_child(?MODULE, Id).

get_childspec(Id) ->
    supervisor:get_childspec(?MODULE, Id).

%% ====================================================================
%% Callback functions
%% ====================================================================

init(Opts) ->
    Domain = ?BU:get_by_key('domain', Opts),
    FacadeRegName = ?BU:to_atom(?BU:get_by_key('regname', Opts)), % crash if not
    %------------
    FacadeSrv = {?DSRV, {?DSRV, start_link, [FacadeRegName, Opts]}, permanent, 1000, worker, [?DSRV]},
    %WorkerSupv = {?DWORKERSUPV, {?DWORKERSUPV, start_link, [Opts]}, permanent, 1000, supervisor, [?DWORKERSUPV]},
    % -----------
    ?LOG('$info', "~ts. '~ts' supv inited as service", [?APP, Domain]),
    {ok, {{one_for_one, 10, 2}, [FacadeSrv]}}.
