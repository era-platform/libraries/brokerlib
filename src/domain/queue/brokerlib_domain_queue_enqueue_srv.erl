%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2022 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 07.09.2022
%%% @doc In-domain specific queue worker (enqueue/dequeue) gen_server
%%%      Opts:
%%%         domain, queuename, queue_meta, sync_ref, type :: 'enqueue' | 'dequeue'

-module(brokerlib_domain_queue_enqueue_srv).
-author('Peter Bukashin <tbotc@yandex.ru>').

-behaviour(gen_server).

-export([start_link/1]).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2, code_change/3]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("app.hrl").

-define(S_Initing, '$initing').
-define(S_InitRetry, '$init_retry').
-define(S_InitError, '$init_error').
-define(S_Starting, '$starting').
-define(S_Normal, '$normal').

%%-record(waitor, {
%%    fun_apply :: function(),
%%    ts :: non_neg_integer(),
%%    timeout :: timeout()
%%}).

-record(state, {
    opts :: list(),
    %
    domain :: binary() | atom(),
    self :: pid(),
    initts :: integer(),
    table_name :: atom(),
    %
    parent_monref :: reference(),
    %
    queuename :: binary(),
    queue_meta :: map(),
    %use_consumer_groups :: boolean(),
    %delete_timeout :: undefined | integer(),
    %delete_count :: undefined | integer(),
    %delete_size :: undefined | integer(),
    %
    sync_ref :: reference(),
    type = 'enqueue',
    %
    last_event_id = 0,
    %
    ref :: reference(),
    timerref :: reference()
}).

%-define(QueueMaxEventId,4000000000).

-define(MaxQueueLen, 2000000).
-define(FreeBufferLen, 10000).

%% ====================================================================
%% API functions
%% ====================================================================

start_link(Opts) ->
    gen_server:start_link(?MODULE, Opts, []).

%% ====================================================================
%% Callback functions
%% ====================================================================

%% ------------------------------
%% Init
%% ------------------------------
init(Opts) ->
    [Domain,QueueName,QueueMeta,SyncRef] =
        ?BU:extract_required_props(['domain','queuename','queue_meta','sync_ref'], Opts),
    % ----
    {Self, Ref} = {self(), make_ref()},
    State = #state{opts = Opts,
                   domain = Domain,
                   table_name = ?MnesiaMeta:table_name(Domain,QueueName),
                   self = Self,
                   ref = Ref,
                   initts = ?BU:timestamp(),
                   sync_ref = SyncRef,
                   %
                   queuename = QueueName,
                   queue_meta = QueueMeta,
                   %use_consumer_groups = maps:get(use_consumer_groups,QueueMeta,false),
                   %delete_timeout = maps:get(delete_timeout,QueueMeta,undefined),
                   %delete_count = maps:get(delete_count,QueueMeta,undefined),
                   %delete_size = maps:get(delete_size,QueueMeta,undefined)
                   %
                   last_event_id = 0
                  },
    % ----
    self() ! {'init',Ref},
    ?LOG('$info', "~ts. '~ts' * '~ts' enqueue srv inited", [?APP, Domain, QueueName]),
    {ok, State}.

%% ------------------------------
%% Call
%% ------------------------------

%% --------------
%% returns current node
%% --------------
handle_call({get_node}, _From, State) ->
    {reply, {node(), self()}, State};

%% --------------
%% print state
%% --------------
handle_call({print}, _From, #state{domain=Domain,queuename=QN,type=Type}=State) ->
    ?OUT('$force', "~ts. '~ts' * '~ts' ~ts srv state: ~n\t~120tp", [?APP, Domain, QN, Type, State]),
    {reply, ok, State};

%% --------------
%% restart gen_serv
%% --------------
handle_call({restart}, _From, State) ->
    {stop, restart, ok, State};

%% --------------
%% other
%% --------------
handle_call(_Msg, _From, State) ->
    {noreply, State}.

%% ------------------------------
%% Cast
%% ------------------------------

%% --------------
%% Enqueue request
%% --------------
handle_cast({enqueue,FunReply,Message}, State) ->
    State1 = enqueue(Message,FunReply,State),
    {noreply, State1};

handle_cast({enqueue_list,FunReply,Messages}, State) ->
    State1 = enqueue_list(Messages,FunReply,State),
    {noreply, State1};

%% --------------
%% other
%% --------------
handle_cast(_Msg, State) ->
    {noreply, State}.

%% ------------------------------
%% Info
%% ------------------------------

%% --------------
%% on start -> db initialization
%% --------------
handle_info({'init',Ref}, #state{ref=Ref,domain=Domain,queuename=QN,sync_ref=SyncRef,type=Type,table_name=Tab,opts=Opts}=State) ->
    MonRef = case ?BU:get_by_key('queue_pid',Opts,undefined) of
                 undefined -> undefined;
                 QueuePid -> erlang:monitor(process,QueuePid)
             end,
    ?QueueSrv:cast(?QueueSrv:get_regname(Domain,QN), {started,SyncRef,QN,Type,self()}),
    {noreply,State#state{last_event_id = get_mnesia_last_key(Tab),
                         parent_monref = MonRef}};

%% --------------
%% DOWN parent queue server (domain)
%% --------------
handle_info({'DOWN',MonRef,process,_,_},#state{parent_monref=MonRef}=State) ->
    {stop, restart, State};

%% --------------
%% other
%% --------------
handle_info(_Info, State) ->
    {noreply, State}.

%% ------------------------------
%% Terminate
%% ------------------------------
terminate(_Reason, #state{}=_State) ->
    ok.

%% ------------------------------
%% Code change
%% ------------------------------
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%% ====================================================================
%% Internal functions
%% ====================================================================

enqueue(Message,FunReply,State) ->
    enqueue_list([Message],FunReply,State).

enqueue_list(Messages,FunReply,#state{domain=Domain,queuename=QN,table_name=Tab,last_event_id=LastEventId}=State) ->
    MessageCount = length(Messages),
    S0 = case mnesia:table_info(Tab, size) of
             Size when Size < ?MaxQueueLen -> Size;
             Size ->
                 DelCount = Size - ?MaxQueueLen + ?FreeBufferLen,
                 ?LOG('$warning', "~ts. '~ts' * '~ts' drop ~p overhead messages", [?APP,Domain,QN,DelCount]),
                 {_,DelCount1,DelKeys} = ?STORE:get_data_pack(Tab,DelCount),
                 ?STORE:del_list_keys(DelKeys,Tab),
                 Size - DelCount1
         end,
    case ?STORE:put_multi(Tab,Messages,LastEventId) of
        {ok,NewEventId} ->
            S1 = mnesia:table_info(Tab, size),
            %SaveEventId = case NewEventId > ?QueueMaxEventId of true -> 0; false -> NewEventId end,
            SaveEventId = NewEventId,
            case S0==0 orelse (S1>0 andalso S1=<MessageCount) of % instead of transaction
                true -> ?QueueSrv:cast(?QueueSrv:get_regname(Domain,QN), wakeup);
                false -> ok
            end,
            FunReply(ok),
            State#state{last_event_id=SaveEventId};
        {error,_}=Err ->
            FunReply(Err),
            State
    end.

%% @private
get_mnesia_last_key(Tab) ->
    case ?STORE:get_last_key(Tab) of
        {_,Key} -> Key
    end.