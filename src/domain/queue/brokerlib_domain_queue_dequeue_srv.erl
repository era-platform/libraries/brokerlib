%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2022 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 07.09.2022
%%% @doc In-domain specific queue worker (enqueue/dequeue) gen_server
%%%      Opts:
%%%         domain, queuename, queue_meta, sync_ref, type :: 'enqueue' | 'dequeue'

-module(brokerlib_domain_queue_dequeue_srv).
-author('Peter Bukashin <tbotc@yandex.ru>').

-behaviour(gen_server).

-export([start_link/1]).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2, code_change/3]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("app.hrl").

-define(S_Initing, '$initing').
-define(S_InitRetry, '$init_retry').
-define(S_InitError, '$init_error').
-define(S_Starting, '$starting').
-define(S_Normal, '$normal').

%%-record(waitor, {
%%    fun_apply :: function(),
%%    ts :: non_neg_integer(),
%%    timeout :: timeout()
%%}).

-record(state, {
    opts :: list(),
    %
    domain :: binary() | atom(),
    self :: pid(),
    initts :: integer(),
    table_name :: atom(),
    %
    parent_monref :: reference(),
    %
    queuename :: binary(),
    queue_meta :: map(),
    use_consumer_groups :: boolean(),
    delete_timeout :: undefined | integer(),
    delete_count :: undefined | integer(),
    delete_size :: undefined | integer(),
    %
    sync_ref :: reference(),
    type = 'dequeue',
    %
    consumers = #{} :: map(), % pid() => Info::map()
    dequeue_tref :: reference(),
    %
    ref :: reference(),
    timerref :: reference()
}).

-define(IntervalCheckEvent, 1000).

%% ====================================================================
%% API functions
%% ====================================================================

start_link(Opts) ->
    gen_server:start_link(?MODULE, Opts, []).

%% ====================================================================
%% Callback functions
%% ====================================================================

%% ------------------------------
%% Init
%% ------------------------------
init(Opts) ->
    [Domain,QueueName,QueueMeta,SyncRef] =
        ?BU:extract_required_props(['domain','queuename','queue_meta','sync_ref'], Opts),
    % ----
    {Self, Ref} = {self(), make_ref()},
    State = #state{opts = Opts,
                   domain = Domain,
                   table_name = ?MnesiaMeta:table_name(Domain,QueueName),
                   self = Self,
                   ref = Ref,
                   initts = ?BU:timestamp(),
                   sync_ref = SyncRef,
                   %
                   queuename = QueueName,
                   queue_meta = QueueMeta,
                   %use_consumer_groups = maps:get(use_consumer_groups,QueueMeta,false),
                   %delete_timeout = maps:get(delete_timeout,QueueMeta,undefined),
                   %delete_count = maps:get(delete_count,QueueMeta,undefined),
                   %delete_size = maps:get(delete_size,QueueMeta,undefined),
                   %
                   consumers = #{}
                  },
    % ----
    self() ! {'init',Ref},
    ?LOG('$info', "~ts. '~ts' * '~ts' dequeue srv inited", [?APP, Domain, QueueName]),
    {ok, State}.

%% ------------------------------
%% Call
%% ------------------------------

%% --------------
%% returns current node
%% --------------
handle_call({get_node}, _From, State) ->
    {reply, {node(), self()}, State};

%% --------------
%% print state
%% --------------
handle_call({print}, _From, #state{domain=Domain,queuename=QN,type=Type}=State) ->
    ?OUT('$force', "~ts. '~ts' * '~ts' ~ts srv state: ~n\t~120tp", [?APP, Domain, QN, Type, State]),
    {reply, ok, State};

%% --------------
%% restart gen_serv
%% --------------
handle_call({restart}, _From, State) ->
    {stop, restart, ok, State};

%% --------------
%% other
%% --------------
handle_call(_Msg, _From, State) ->
    {noreply, State}.

%% ------------------------------
%% Cast
%% ------------------------------

%% --------------
%% Register consumer request
%% OptsMap :: #{pid, wakeup_message, rolename, [group]}
%% -> {ok,Ref,MonitorPid}
%% --------------
handle_cast({register_consumer,FunReply,OptsMap}, #state{queuename=QN,consumers=Consumers}=State) when is_map(OptsMap) ->
    State1 = case maps:get(pid,OptsMap,undefined) of
                 SubscrPid when is_pid(SubscrPid) ->
                     case maps:get({pid,SubscrPid},Consumers,undefined) of
                         undefined ->
                             ?LOG('$trace',"Register consumer q='~ts': ~120tp", [QN,OptsMap]),
                             Ref = make_ref(),
                             Consumers1 = Consumers#{{pid,SubscrPid} => OptsMap#{ref => Ref},
                                                     {ref,Ref} => OptsMap#{ref => Ref}},
                             _MonRef = erlang:monitor(process,SubscrPid),
                             FunReply({ok,Ref,self()}),
                             gen_server:cast(self(), wakeup),
                             State#state{consumers=Consumers1};
                        OldConsumer ->
                            ?LOG('$trace',"Reregister consumer q='~ts': ~120tp", [QN,OptsMap]),
                            Ref = maps:get(ref,OldConsumer),
                            FunReply({ok,Ref,self()}),
                            gen_server:cast(self(), wakeup),
                            State
                     end;
                 _ ->
                     ?LOG('$trace',"Invalid register message q='~ts', invalid consumer subscriber pid: ~120tp", [QN,OptsMap]),
                     State
             end,
    {noreply,State1};

%% --------------
%% Dequeue request
%% OptsMap :: #{ref, count, [next_id]}
%% --------------
handle_cast({dequeue,FunReply,OptsMap}, State) ->
    State1 = dequeue(OptsMap,FunReply,State),
    {noreply, State1};

%% --------------
%% Peek request
%% OptsMap :: #{ref, [next_id]}
%% --------------
handle_cast({peek,FunReply,OptsMap}, State) ->
    State1 = peek(OptsMap,FunReply,State),
    {noreply, State1};

%% --------------
%% Delete request
%% OptsMap :: #{ref, key}
%% --------------
handle_cast({delete,FunReply,OptsMap}, State) ->
    State1 = delete(OptsMap,FunReply,State),
    {noreply, State1};

%% --------------
%% Wake up on queue unempty from enqueue pid
%% --------------
handle_cast(wakeup, #state{table_name=Tab,queuename=QN,consumers=Consumers}=State) ->
    case mnesia:table_info(Tab, size)>0 of
        true -> send_wakeup_event(QN,Consumers);
        false -> ok
    end,
    {noreply, State#state{dequeue_tref=undefined}};

%% --------------
%% other
%% --------------
handle_cast(_Msg, State) ->
    {noreply, State}.

%% ------------------------------
%% Info
%% ------------------------------

%% --------------
%% on start -> db initialization
%% --------------
handle_info({'init',Ref}, #state{ref=Ref,domain=Domain,queuename=QN,sync_ref=SyncRef,type=Type,opts=Opts}=State) ->
    MonRef = case ?BU:get_by_key('queue_pid',Opts,undefined) of
                 undefined -> undefined;
                 QueuePid -> erlang:monitor(process,QueuePid)
             end,
    ?QueueSrv:cast(?QueueSrv:get_regname(Domain,QN), {started,SyncRef,QN,Type,self()}),
    {noreply,State#state{parent_monref = MonRef}};

%% --------------
%% wakeup self
%% --------------
handle_info({wakeup,Ref}, #state{ref=Ref,table_name=TableName,queuename=QN,consumers=Consumers}=State) ->
    case mnesia:table_info(TableName, size)>0 of
        true -> send_wakeup_event(QN,Consumers);
        false -> ok
    end,
    {noreply, State#state{dequeue_tref=undefined}};

%% --------------
%% DOWN parent queue server (domain)
%% --------------
handle_info({'DOWN',MonRef,process,_,_},#state{parent_monref=MonRef}=State) ->
    {stop, restart, State};

%% --------------
%% DOWN consumer
%% --------------
handle_info({'DOWN',_MonRef,process,Pid,_},#state{queuename=QN,consumers=Consumers}=State) ->
    case maps:get({pid,Pid},Consumers,undefined) of
        undefined -> {noreply,State};
        Consumer when is_map(Consumer) ->
            ?LOG('$trace',"Found consumer down q='~ts': ~120tp", [QN,Pid]),
            Ref = maps:get(ref,Consumer),
            {noreply,State#state{consumers=maps:without([{pid,Pid},{ref,Ref}],Consumers)}}
    end;

%% --------------
%% other
%% --------------
handle_info(_Info, State) ->
    {noreply, State}.

%% ------------------------------
%% Terminate
%% ------------------------------
terminate(_Reason, #state{}=_State) ->
    ok.

%% ------------------------------
%% Code change
%% ------------------------------
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% @private
send_wakeup_event(QueueName,Consumers) ->
    F = fun() ->
            Fx = fun({pid,ConsumerPid},OptsMap) when is_pid(ConsumerPid) ->
                        WakeupMsg = maps:get('wakeup_message',OptsMap,{'wakeup',QueueName}),
                        ConsumerPid ! WakeupMsg;
                    (_,_) -> ok
                 end,
            maps:map(Fx,Consumers)
        end,
    %erlang:spawn(F).
    F().

%% -----------------------------------------------------------
%% Dequeue
%% -----------------------------------------------------------
dequeue(OptsMap,FunReply,#state{}=State) ->
    Ref = ?BU:get_by_key(ref,OptsMap,undefined),
    Cnt = ?BU:get_by_key(count,OptsMap,1),
    do_dequeue(OptsMap,FunReply,Cnt,Ref,State).

%% @private
do_dequeue(_,FunReply,0,Ref,#state{ref=Ref}=State) ->
    FunReply({ok,[]}),
    State;
do_dequeue(_OptsMap,FunReply,Cnt,Ref,#state{table_name=Tab,dequeue_tref=TRef,consumers=Consumers}=State) ->
    case maps:fold(fun({ref,X},Consumer,false) when X==Ref -> Consumer; (_,_,Acc) -> Acc end, false, Consumers) of
        false ->
            FunReply({error,{access_denied,<<"Unauthorized">>}}),
            State;
        _ ->
            % TODO: come up with collision of returning same keys for several replicas working in parallel
            {Data,CntData,Keys} = ?STORE:get_data_pack(Tab,Cnt),
            FunReply({ok,Data}),
            ?STORE:del_list_keys(Keys,Tab),
            ?BU:cancel_timer(TRef),
            TRef1 = case CntData of
                        0 -> undefined;
                        _ -> erlang:send_after(?IntervalCheckEvent,self(),{wakeup,Ref})
                    end,
            State#state{dequeue_tref=TRef1}
    end.


%% -----------------------------------------------------------
%% Peek
%% -----------------------------------------------------------
peek(OptsMap,FunReply,#state{}=State) ->
    Ref = ?BU:get_by_key(ref,OptsMap,undefined),
    do_peek(OptsMap,FunReply,Ref,State).

%% @private
do_peek(_OptsMap,FunReply,Ref,#state{table_name=Tab,dequeue_tref=TRef,consumers=Consumers}=State) ->
    case maps:fold(fun({ref,X},Consumer,false) when X==Ref -> Consumer; (_,_,Acc) -> Acc end, false, Consumers) of
        false ->
            FunReply({error,{access_denied,<<"Unauthorized">>}}),
            State;
        _ ->
            ?BU:cancel_timer(TRef),
            % TODO: come up with collision of returning same keys for several replicas working in parallel
            case ?STORE:get_data_pack(Tab,1) of
                {[Data],_CntData,[Key]} ->
                    FunReply({ok,Data,Key}),
                    TRef1 = erlang:send_after(?IntervalCheckEvent,self(),{wakeup,Ref}),
                    State#state{dequeue_tref=TRef1};
                {[],0,[]} ->
                    FunReply(false),
                    State
            end
    end.

%% -----------------------------------------------------------
%% Delete
%% -----------------------------------------------------------
delete(OptsMap,FunReply,#state{}=State) ->
    Ref = ?BU:get_by_key(ref,OptsMap,undefined),
    Key = ?BU:get_by_key(key,OptsMap,undefined),
    do_delete(OptsMap,FunReply,Key,Ref,State).

%% @private
do_delete(_,FunReply,undefined,Ref,#state{ref=Ref}=State) ->
    FunReply(false),
    State;
do_delete(_OptsMap,FunReply,Key,Ref,#state{table_name=Tab,consumers=Consumers}=State) ->
    case maps:fold(fun({ref,X},Consumer,false) when X==Ref -> Consumer; (_,_,Acc) -> Acc end, false, Consumers) of
        false ->
            FunReply({error,{access_denied,<<"Unauthorized">>}}),
            State;
        _ ->
            % TODO: come up with collision of returning same keys for several replicas working in parallel
            ok = ?STORE:del([Key,Tab]),
            FunReply(ok),
            State
    end.


