%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 12.03.2021
%%% @doc In-domain specific queue facade gen_server
%%%      Opts:
%%%         domain, queuename, queue_meta, sync_ref

-module(brokerlib_domain_queue_srv).
-author('Peter Bukashin <tbotc@yandex.ru>').

-behaviour(gen_server).

-export([start_link/1, get_regname/2, cast/3, cast/2]).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2, code_change/3]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("app.hrl").

-define(RetryStartTimeout,60000+?BU:random(5000)).
-define(CacheFilterTimeout,60000+?BU:random(5000)).
-define(CheckQueueSizePortion,100). % how many crud operations before check size
-define(OverloadedQueueSize,20). % size of queue to pause class with service unavailable until queue is 0 (max queue of workers, readers, notifier).

-define(S_Initing, '$initing').
-define(S_InitRetry, '$init_retry').
-define(S_InitError, '$init_error').
-define(S_Starting, '$starting').
-define(S_Normal, '$normal').

-record(waitor, {
    fun_apply :: function(),
    ts :: non_neg_integer(),
    timeout :: timeout()
}).

-record(qstate, {
    opts :: list(),
    %
    regname :: atom(),
    domain :: binary() | atom(),
    self :: pid(),
    initts :: integer(),
    %
    queuename :: binary(),
    queue_meta :: map(),
    sync_ref :: reference(),
    %
    ref :: reference(),
    timerref :: reference(),
    %
    status = '&initing' :: '$initing' | '$init_retry' | '$init_error' | '$starting' | '$normal',
    workers = {undefined,undefined},
    reason :: term(),
    %
    waitors = [] :: [Waitor::#waitor{}]
}).

%% ====================================================================
%% API functions
%% ====================================================================

start_link(Opts) ->
    Domain = ?BU:get_by_key('domain',Opts),
    QueueName = ?BU:get_by_key('queuename',Opts),
    RegName = get_regname(Domain,QueueName),
    gen_server:start_link({local, RegName}, ?MODULE, [{'regname', RegName}|Opts], []).

%% returns name of supv for linking to top supv
get_regname(Domain,QueueName) when is_binary(Domain); is_atom(Domain) ->
    ?BU:to_atom_new(?BU:strbin("~ts:srv;dom:~ts;qn:~ts",[?APP,Domain,QueueName])).

cast(Domain,QueueName,Message) ->
    gen_server:cast(get_regname(Domain,QueueName),Message).
cast(RegName,Message) when is_atom(RegName) ->
    gen_server:cast(RegName,Message).

%% ====================================================================
%% Callback functions
%% ====================================================================

%% ------------------------------
%% Init
%% ------------------------------
init(Opts) ->
    [RegName,Domain,QueueName,QueueMeta,SyncRef] =
        ?BU:extract_required_props(['regname','domain','queuename','queue_meta','sync_ref'], Opts),
    % ----
    {Self, Ref} = {self(), make_ref()},
    State = #qstate{opts = Opts,
                    regname = RegName,
                    domain = Domain,
                    self = Self,
                    ref = Ref,
                    initts = ?BU:timestamp(),
                    queuename = QueueName,
                    queue_meta = QueueMeta,
                    sync_ref = SyncRef,
                    status = ?S_Initing},
    % ----
    self() ! {'init',Ref},
    ?LOG('$info', "~ts. '~ts' * '~ts' facade inited as '~ts'", [?APP, Domain, QueueName, RegName]),
    {ok, State}.

%% ------------------------------
%% Call
%% ------------------------------

%% --------------
%% returns current node
handle_call({get_node}, _From, State) ->
    {reply, {node(), self()}, State};

%% --------------
%% print state
handle_call({print}, _From, #qstate{domain=Domain,queuename=QN}=State) ->
    ?OUT('$force', "~ts. '~ts' * '~ts' facade state: ~n\t~120tp", [?APP, Domain, QN, State]),
    {reply, ok, State};

%% --------------
%% restart gen_serv
handle_call({restart}, _From, State) ->
    {stop, restart, ok, State};

%% --------------
%% other
handle_call(_Msg, _From, State) ->
    {noreply, State}.

%% ------------------------------
%% Cast
%% ------------------------------

handle_cast({started,SyncRef,QN,Type,Pid},#qstate{sync_ref=SyncRef,domain=Domain,queuename=QN,workers=Wrk,status=Status}=State) ->
    Wrk1 = case Type of
               'enqueue' -> setelement(1,Wrk,Pid);
               'dequeue' -> setelement(2,Wrk,Pid)
           end,
    Status1 = case Wrk1 of
                  {undefined,_} -> Status;
                  {_,undefined} -> Status;
                  _ -> ?S_Normal
              end,
    ?DSRV:cast(?DSRV:get_regname(Domain), {started,SyncRef,QN,Type,Pid}),
    {noreply,State#qstate{workers=Wrk1,
                          status=Status1}};

%%
handle_cast(wakeup,#qstate{workers={_,WrkDequeue}}=State) when is_pid(WrkDequeue) ->
    gen_server:cast(WrkDequeue, wakeup),
    {noreply,State};

%% --------------
%% other
%% --------------
handle_cast(_Msg, State) ->
    {noreply, State}.

%% ------------------------------
%% Info
%% ------------------------------

%% --------------
%% on start -> db initialization
%% --------------
handle_info({'init',Ref}, #qstate{ref=Ref,status=?S_Initing,queue_meta=QueueMeta}=State) ->
    State1 = pause_at_start(State),
    State2 = do_init(State1#qstate{queue_meta = organize_meta(QueueMeta)}),
    {noreply,State2};
handle_info({'init',Ref}, #qstate{ref=Ref}=State) ->
    State1 = do_init(State#qstate{status=?S_Initing}),
    {noreply,State1};

%% --------------
%% other
%% --------------
handle_info(_Info, State) ->
    {noreply, State}.

%% ------------------------------
%% Terminate
%% ------------------------------
terminate(_Reason, #qstate{}=_State) ->
    ok.

%% ------------------------------
%% Code change
%% ------------------------------
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% ------------------------------------------------
%% Pause to avoid high load on cycled fall
%% ------------------------------------------------
pause_at_start(#qstate{domain=Domain,queuename=QN}=State) ->
    Key = {start_ts,?MODULE,Domain,QN},
    case ?BLstore:find_t(Key) of
        {_,_TS} ->
            ?LOG('$info', "~ts. '~ts' * '~ts'. Pause at start...", [?APP,Domain,QN]),
            timer:sleep(1000);
        _ -> ok
    end,
    ?BLstore:store_t(Key,?BU:timestamp(),1000),
    State.

%% ------------------------------------------------
%% Organize meta by caches
%% ------------------------------------------------
organize_meta(QueueMeta) -> QueueMeta.

%% ------------------------------------------------
%% When class started, it should initialize storage
%% ------------------------------------------------
do_init(#qstate{domain=Domain,queuename=QN,queue_meta=Meta}=State) ->
    ?LOG('$trace', "~ts. '~ts' * '~ts'. Initing...", [?APP,Domain,QN]),
    case ?MnesiaMeta:ensure_table(Domain,QN,Meta) of
        {error,Reason} ->
            Ref1 = make_ref(),
            State#qstate{status=?S_InitError,
                         reason=Reason,
                         ref=Ref1,
                         timerref=erlang:send_after(30000,self(),{'init',Ref1})};
        {retry_after,Timeout,Reason} ->
            Ref1 = make_ref(),
            State#qstate{status=?S_InitRetry,
                         reason=Reason,
                         ref=Ref1,
                         timerref=erlang:send_after(Timeout,self(),{'init',Ref1})};
        ok ->
            start_pids(State),
            State#qstate{status=?S_Starting,
                         reason=undefined}
    end.

%% @private
start_pids(#qstate{domain=Domain,queuename=QN,opts=Opts}) ->
    %{?WrkSupv, {?WrkSupv, start_link, [Opts1]}, permanent, 1000, supervisor, [?WrkSupv]},
    Opts1 = [{'queue_pid',self()} | Opts],
    EnqueueChild = {'enqueue', {?EnqueueSrv, start_link, [Opts1]}, permanent, 1000, worker, [?EnqueueSrv]},
    DequeueChild = {'dequeue', {?DequeueSrv, start_link, [Opts1]}, permanent, 1000, worker, [?DequeueSrv]},
    RegName = ?QueueSupv:get_regname(Domain,QN),
    ?QueueSupv:start_child(RegName,EnqueueChild),
    ?QueueSupv:start_child(RegName,DequeueChild).

%%%% -----------------------------
%%%% @private
%%log_loaded(#qstate{domain=Domain,queuename=QN}) ->
%%    ?LOG('$info', "~ts. '~ts' * '~ts'. Queue loaded!", [?APP,Domain,QN]).
%%
%%%% -----------------------------
%%%% @private
%%%% Add call request to waitors of class load
%%append_waitor(FunApply,SyncTimeout,#qstate{waitors=W}=State) ->
%%    NowTS = ?BU:timestamp(),
%%    Waitor = #waitor{fun_apply = FunApply,
%%                     timeout = SyncTimeout,
%%                     ts = NowTS},
%%    W1 = lists:filter(fun(#waitor{ts=TS,timeout=Timeout}) -> TS+Timeout > NowTS end, W),
%%    State#qstate{waitors=[Waitor|W1]}.
%%
%%%% Apply waitors when something changed in classes list
%%apply_waitors(#qstate{waitors=W}=State) ->
%%    NowTS = ?BU:timestamp(),
%%    State1 = lists:foldl(fun(#waitor{ts=TS,timeout=Timeout},StateX) when TS+Timeout>=NowTS -> StateX;
%%                            (#waitor{fun_apply=FunApply},StateX) -> FunApply(), StateX
%%                         end, State, W),
%%    State1#qstate{waitors=[]}.
