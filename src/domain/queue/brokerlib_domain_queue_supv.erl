%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 16.03.2021
%%% @doc In-domain queue supervisor
%%%      Opts:
%%%         domain, queuename, queue_meta

-module(brokerlib_domain_queue_supv).
-author('Peter Bukashin <tbotc@yandex.ru>').

-behaviour(supervisor).

-export([start_link/1, get_regname/2, get_linkname/2]).
-export([start_child/2, restart_child/2, terminate_child/2, delete_child/2, get_childspec/2]).
-export([init/1]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

start_link(Opts) ->
    Domain = ?BU:get_by_key('domain',Opts),
    QueueName = ?BU:get_by_key('queuename',Opts),
    supervisor:start_link({local,get_regname(Domain,QueueName)},?MODULE, Opts).

get_regname(Domain,QueueName) when is_binary(Domain); is_atom(Domain) ->
    ?BU:to_atom_new(?BU:strbin("~ts:supv;dom:~ts;qn:~ts",[?APP,Domain,QueueName])).

%% returns name of supv for linking to top supv
get_linkname(Domain,QueueName) when is_binary(Domain); is_atom(Domain) ->
    ?BU:to_atom_new(?BU:strbin("~ts:supv;dom:~ts;qn:~ts",[?APP,Domain,QueueName])).

%% ====================================================================
%% API functions (ext management)
%% ====================================================================

start_child(RegName,ChildSpec) ->
    supervisor:start_child(RegName, ChildSpec).

restart_child(RegName,Id) ->
    supervisor:restart_child(RegName, Id).

terminate_child(RegName,Id) ->
    supervisor:terminate_child(RegName, Id).

delete_child(RegName,Id) ->
    supervisor:delete_child(RegName, Id).

get_childspec(RegName,Id) ->
    supervisor:get_childspec(RegName, Id).

%% ====================================================================
%% Callback functions
%% ====================================================================

init(Opts) ->
    Domain = ?BU:get_by_key('domain',Opts),
    QueueName = ?BU:get_by_key('queuename',Opts),
    SyncRef = make_ref(),
    Opts1 = [{'sync_ref',SyncRef} | Opts],
    ChildSpec = [{?QueueSrv, {?QueueSrv, start_link, [Opts1]}, permanent, 1000, worker, [?QueueSrv]}],
    ?LOG('$info', "~ts. '~ts' * '~ts' supv inited", [?APP, Domain, QueueName]),
    {ok, {{one_for_one, 10, 2}, ChildSpec}}.