%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2022 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 06.09.2022
%%% @doc Configuration functions

-module(brokerlib_config).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([
    log_destination/1,
    schema_nodes/1,
    % unused
    transaction_mode/0,
    write_lock_kind/0
]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% return log destination for loggerlib, ex. {app,prefix}
log_destination(Default) ->
    ?BU:get_env(?APP,'log_destination',Default).

%% return schema_nodes from opts
schema_nodes(Default) ->
    ?BU:get_env(?APP,'schema_nodes',Default).

%% return transaction_mode
transaction_mode() ->
    Default = <<"transaction">>, % transaction | dirty
    ?BU:get_env(?APP,'transaction_mode',Default).

%% return write_lock_kind
write_lock_kind() ->
    Default = <<"sticky_write">>, % write | sticky_write
    ?BU:get_env(?APP,'is_dirty',Default).

%% ====================================================================
%% Internal functions
%% ====================================================================