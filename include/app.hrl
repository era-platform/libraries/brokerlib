

%% ====================================================================

-record(dstate, {
    regname :: atom(),
    domain :: binary(),
    queues = #{} :: map(), % #{QueueName => {EnqueuePid,DequeuePid}}
    %
    static_items = [] :: [map()],
    static_hc = -1 :: integer(),
    %
    statref :: reference(),
    stattimerref :: reference(),
    %
    waitors = [] :: [Waitor::term()]
}).

%% ====================================================================


-define(BASICLIB, basiclib).
-define(LOGGERLIB, loggerlib).
-define(MNESIALIB, mnesialib).

-define(APP, brokerlib).

-define(SUPV, brokerlib_supv).
-define(CFG, brokerlib_config).

-define(DSUPV, brokerlib_domain_supv).
-define(DSRV, brokerlib_domain_srv).
-define(ModelCfg, brokerlib_domain_model_config).

-define(QueueSupv, brokerlib_domain_queue_supv).
-define(QueueSrv, brokerlib_domain_queue_srv).
-define(EnqueueSrv, brokerlib_domain_queue_enqueue_srv).
-define(DequeueSrv, brokerlib_domain_queue_dequeue_srv).

-define(MnesiaMeta, brokerlib_mnesia_meta).
-define(STORE, brokerlib_storage_dirty).

%% -------------
-define(BU, basiclib_utils).
-define(BLlog, basiclib_log).
-define(BLstore, basiclib_store).

%% ====================================================================
%% Define logs
%% ====================================================================

-define(LOGFILE, {env,?APP}).

-define(LOG(Level,Fmt,Args), ?BLlog:write(Level, ?CFG:log_destination(?LOGFILE), {Fmt,Args})).
-define(LOG(Level,Text), ?BLlog:write(Level, ?CFG:log_destination(?LOGFILE), Text)).

-define(OUT(Level,Fmt,Args), ?BLlog:writeout(Level, ?CFG:log_destination(?LOGFILE), {Fmt,Args})).
-define(OUT(Level,Text), ?BLlog:writeout(Level, ?CFG:log_destination(?LOGFILE), Text)).

%% ====================================================================
%% Define other
%% ====================================================================

-define(TablePrefix, "broker__").